#!/usr/bin/env bash

source ./.env

#docker stop crpmango_standard_wordpress crpmango_dbmaster_standard
# docker stop $(docker ps -a -q) && docker rm $(docker ps -aq) -f

docker stop ${COMPOSE_PROJECT_NAME}_${PHPVERSION} ${COMPOSE_PROJECT_NAME}_healthcheck ${COMPOSE_PROJECT_NAME}_wpcli ${COMPOSE_PROJECT_NAME}_phpmyadmin ${COMPOSE_PROJECT_NAME}_${DATABASE} ${COMPOSE_PROJECT_NAME}_${DATABASE}_slave ${COMPOSE_PROJECT_NAME}_mailhog && docker rm ${COMPOSE_PROJECT_NAME}_${PHPVERSION} ${COMPOSE_PROJECT_NAME}_healthcheck ${COMPOSE_PROJECT_NAME}_wpcli ${COMPOSE_PROJECT_NAME}_phpmyadmin ${COMPOSE_PROJECT_NAME}_${DATABASE} ${COMPOSE_PROJECT_NAME}_${DATABASE}_slave ${COMPOSE_PROJECT_NAME}_mailhog -f