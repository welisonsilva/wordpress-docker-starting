#!/bin/bash
# Local .env
source ./.env

# Corrigindo Problema de usuario slave, não existir quando restartar o docker 

echo "Iniciando MASTER > SLAVE"

echo "Criando usuario (${SLAVEUSERDBREAD}) para ser usado em db-config.php"
docker exec -it ${COMPOSE_PROJECT_NAME}_${DATABASE}_slave mysql -u root -p$MYSQL_PASSWORD -e "
# Exibir tabelas
SHOW DATABASES;

# criar usuário
CREATE USER '${SLAVEUSERDBREAD}' IDENTIFIED BY '${MYSQL_PASSWORD}';

# Exibir usuários criados
SELECT User FROM mysql.user;

# Permissão para usuário criado
GRANT USAGE ON *.* TO '${SLAVEUSERDBREAD}'@'%' IDENTIFIED BY '${MYSQL_PASSWORD}';

# Mudar permissão do usuário criado para apenas leitura (SELEC)
GRANT SELECT ON ${MYSQL_DATABASE}.* TO ${SLAVEUSERDBREAD}@'%' IDENTIFIED BY '${MYSQL_PASSWORD}';

# Aplicar privilégios
FLUSH PRIVILEGES;

# Visualizar privilégios do usuario
SHOW GRANTS FOR '${SLAVEUSERDBREAD}'@'%';
"

echo "Finalizado, usuário: ${SLAVEUSERDBREAD} / senha: ${MYSQL_PASSWORD}."