#!/bin/bash
# Local .env
source ./.env

read -p "O nome do banco de dados utilizado é '$MYSQL_DATABASE', gostaria de alterar?: " dbname
DATABASENAME=${dbname:-$MYSQL_DATABASE}
# type="dev"

echo 'Sufixo para DEV ou PROD?'
select type in dev prod
do
        case $type in 
        dev|prod)   
                break
                ;;
        *)
        echo "Opção inválida" 
        ;;
        esac
done


echo 'O tipo de arquivo SQL ou GZIP?'
select typezip in sql sql.gz
do
        case $typezip in 
        sql|sql.gz)   
                break
                ;;
        *)
        echo "Opção inválida" 
        ;;
        esac
done

nameBD="./bd/${DATABASENAME}_${type}.${typezip}"

if [ $typezip = "sql" ]; then
        # Arquivo SQL
        docker exec ${COMPOSE_PROJECT_NAME}_${DATABASE} /usr/bin/mysqldump -u $MYSQL_USER --password=$MYSQL_PASSWORD --add-drop-table $MYSQL_DATABASE > $nameBD
        # Backup slave
        # docker exec ${COMPOSE_PROJECT_NAME}_${DATABASE} /usr/bin/mysqldump -u root --password=$MYSQL_PASSWORD --all-databases --master-data=1 > $nameBD
else
        # Arquivo SQL Zip
        docker exec ${COMPOSE_PROJECT_NAME}_${DATABASE} /usr/bin/mysqldump -u $MYSQL_USER --password=$MYSQL_PASSWORD --add-drop-table $MYSQL_DATABASE | gzip > $nameBD
fi
echo "Arquivo '${nameBD}' gerado com sucesso!"

