#!/bin/bash
# Local .env
source ./.env

HYPERCACHE=./.docker/config
DB_FILE="${HYPERCACHE}/hyperdb/db.php"
DBCONFIG_FILE="${HYPERCACHE}/hyperdb/db-config.php"

if [[ ! -f "$DBCONFIG_FILE" && ! -f "$DB_FILE" ]]; then
    echo "Baixando arquivos HyperBD"
    sudo apt-get set up unzip -y
    wget -O ${HYPERCACHE}/hyperdb.zip https://downloads.wordpress.org/plugin/hyperdb.zip
    unzip ${HYPERCACHE}/hyperdb.zip -d ${HYPERCACHE}
    sudo rm ${HYPERCACHE}/hyperdb.zip
fi

if [[ ! -f ${DOCUMENT_ROOT}/wp-config.php && ! -d ${DOCUMENT_ROOT}/wp-content ]]; then
    echo "Parece que Wordpress não está instalado. Execute o comando ./autoinstall.sh"
    exit
fi

echo "Iniciando MASTER > SLAVE"

echo "Criando usuario (${SLAVEUSERDBREAD}) para ser usado em db-config.php"
docker exec -it ${COMPOSE_PROJECT_NAME}_${DATABASE}_slave mysql -u root -p$MYSQL_PASSWORD -e "
# Exibir tabelas
SHOW DATABASES;

# criar usuário
CREATE USER '${SLAVEUSERDBREAD}' IDENTIFIED BY '${MYSQL_PASSWORD}';

# Exibir usuários criados
#SELECT User FROM mysql.user;

# Permissão para usuário criado
GRANT USAGE ON *.* TO '${SLAVEUSERDBREAD}'@'%' IDENTIFIED BY '${MYSQL_PASSWORD}';

# Mudar permissão do usuário criado para apenas leitura (SELEC)
GRANT SELECT ON ${MYSQL_DATABASE}.* TO ${SLAVEUSERDBREAD}@'%' IDENTIFIED BY '${MYSQL_PASSWORD}';

# Aplicar privilégios
FLUSH PRIVILEGES;

# Visualizar privilégios do usuario
SHOW GRANTS FOR '${SLAVEUSERDBREAD}'@'%';
"
echo "Configurando..."
docker exec -it ${COMPOSE_PROJECT_NAME}_${DATABASE} mysql -u root -p$MYSQL_PASSWORD -e "
#SHOW DATABASES; 
#SHOW GLOBAL VARIABLES WHERE Variable_name = 'log_bin';
GRANT REPLICATION SLAVE ON *.* TO '${SLAVEUSERREPLICATION}'@'%' IDENTIFIED BY '${MYSQL_PASSWORD}';
FLUSH PRIVILEGES;
SHOW MASTER STATUS;
"

echo "Backup do banco de dados (${MYSQL_DATABASE}) de MASTER"
docker exec ${COMPOSE_PROJECT_NAME}_${DATABASE} /usr/bin/mysqldump -u root --password=$MYSQL_PASSWORD --all-databases --master-data=1 > "./bd/${MYSQL_DATABASE}_master_to_slave_dev.sql"

echo "Importar banco de dados (${MYSQL_DATABASE}) para SLAVE"
docker exec -i ${COMPOSE_PROJECT_NAME}_${DATABASE}_slave mysql -u root --password=$MYSQL_PASSWORD $MYSQL_DATABASE < "./bd/${MYSQL_DATABASE}_master_to_slave_dev.sql"

echo "Configurando SLAVE"
CHANGEMASTERGREP=`head -n 100 "./bd/${MYSQL_DATABASE}_master_to_slave_dev.sql" | grep "^CHANGE MASTER"`
CHANGEMASTER=${CHANGEMASTERGREP/;/,} # Trocar ';' por ',';
CHANGEMASTERCONFIG="${CHANGEMASTER} MASTER_HOST='mysql', MASTER_USER='${SLAVEUSERREPLICATION}', MASTER_PASSWORD='${MYSQL_PASSWORD}'"
echo ${CHANGEMASTERCONFIG};

echo "Conectnado ao SLAVE e configurar"
docker exec -it ${COMPOSE_PROJECT_NAME}_${DATABASE}_slave mysql -u root -p$MYSQL_PASSWORD -e "${CHANGEMASTERCONFIG}; START SLAVE; SHOW SLAVE STATUS \G"

echo "------------------------------------------------";

echo "Copiando o arquivos db-config.php e db.php para a pasta ${DOCUMENT_ROOT}"
sudo cp ${HYPERCACHE}/hyperdb/db-config.php ${DOCUMENT_ROOT}/db-config.php
sudo cp ${HYPERCACHE}/hyperdb/db.php ${DOCUMENT_ROOT}/wp-content/

echo "Finalizado, agora procure o arquivo db-config.php na pasta ${DOCUMENT_ROOT}, e verifique e/ou adicione usuário: ${SLAVEUSERDBREAD} e senha: ${MYSQL_PASSWORD} para o acesso leitura."