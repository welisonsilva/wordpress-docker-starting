
# Inspiração

Setup de instalação WordPress usando Docker para desenvolvimento local em Windows, Ubuntu. *(MAC não testado)*.
## Referencias
- https://www.datanovia.com/en/lessons/wordpress-docker-setup-files-example-for-local-development/

# Iniciando

## Necessário
```bash
sudo apt install pv
sudo apt install make
```

## Comandos
```bash
# Inicia o Docker e instala o wordpress, versão configurada no .env
./autoinstall.sh
```
```bash
# Inicia o Docker
./start.sh
```
```bash
# Para o Docker
./stop.sh
```
```bash
# Importa o banco de dados que está dentro da pasta ./bd para o Docker
./update-db.sh
```
```bash
# Exporta o banco de dados para dentro da pasta ./bd
./dump-db.sh
```
```bash
# Para o Docker e apaga tudo
./clear.sh
```
```bash
# Configura o Replicar BD Master > Slave (ref.: http://www.devin.com.br/replicacao-mysql/)
./dbslave.sh 
```


## Conectar ao docker 
docker exec -it portaluniversal_php74 bash

# Solução para alguns problemas encontrados

[Soluções.md](SOLUCOES.md)


# Dicas
## Parar e Remover todos os container abertos
```bash
docker stop $(docker ps -a -q) && docker rm $(docker ps -aq) -f
```

## Permissão para usuario atual
```bash
sudo chown -R $USER:$USER www
```




