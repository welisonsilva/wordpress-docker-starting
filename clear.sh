#!/usr/bin/env bash

source ./.env


echo "Deseja mesmo DELETAR todos os dados do projeto $COMPOSE_PROJECT_NAME?"

select option in sim nao
do
        case $option in 
        sim|nao)   
                break
                ;;
        *)
            echo "Opção invalida" 
            ;;
        esac
done

if [ $option = "sim" ]; then
    echo "Parando projetos $COMPOSE_PROJECT_NAME no Docker?"
    docker stop ${COMPOSE_PROJECT_NAME}_${PHPVERSION} ${COMPOSE_PROJECT_NAME}_healthcheck ${COMPOSE_PROJECT_NAME}_wpcli ${COMPOSE_PROJECT_NAME}_phpmyadmin ${COMPOSE_PROJECT_NAME}_${DATABASE} ${COMPOSE_PROJECT_NAME}_${DATABASE}_slave ${COMPOSE_PROJECT_NAME}_mailhog && docker rm ${COMPOSE_PROJECT_NAME}_${PHPVERSION} ${COMPOSE_PROJECT_NAME}_healthcheck ${COMPOSE_PROJECT_NAME}_wpcli ${COMPOSE_PROJECT_NAME}_phpmyadmin ${COMPOSE_PROJECT_NAME}_${DATABASE} ${COMPOSE_PROJECT_NAME}_${DATABASE}_slave ${COMPOSE_PROJECT_NAME}_mailhog -f
    
    # Removing related folders/files...
    echo "Removendo arquivos relacionado ao projeto $COMPOSE_PROJECT_NAME?"
    sudo rm -rf www 
    sudo rm -rf .docker/data/mysql 
    sudo rm -rf .docker/data/slave 
    sudo rm -rf .docker/logs/apache2 
    sudo rm -rf .docker/logs/mysql 
    sudo rm -rf .docker/logs/slave 
    sudo rm -rf .docker/cache/wp_cli 
    mkdir www

    echo "✔️ 🔥 Arquivos deletados com sucesso!";
else
  echo "✔️  Nenhum arquivo deletado";
fi