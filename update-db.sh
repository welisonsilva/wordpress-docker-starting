#!/bin/bash
# Local .env
source ./.env

read -p "O nome do banco que será importado é $MYSQL_DATABASE, você quer mudar?: " dbname
DATABASENAME=${dbname:-$MYSQL_DATABASE}
# type="dev"

echo 'Sufixo para DEV ou PROD?'

select type in dev prod
do
        case $type in 
        dev|prod)   
                break
                ;;
        *)
            echo "Opção inválida" 
            ;;
        esac
done

echo 'O tipo de arquivo SQL ou GZIP?'
select typezip in sql sql.gz
do
        case $typezip in 
        sql|sql.gz)   
                break
                ;;
        *)
            echo "Opção inválida"  
            ;;
        esac
done

FILE="./bd/${DATABASENAME}_${type}.${typezip}"
echo $FILE;

if [ -f "$FILE" ]; then
    echo "Importando arquivo $FILE"

    if [ $typezip = "sql" ]; then
        # Arquivo SQL
        docker exec -i ${COMPOSE_PROJECT_NAME}_${DATABASE} mysql -u $MYSQL_USER --password=$MYSQL_PASSWORD $MYSQL_DATABASE < $FILE
        # Banco de dados Slave
        #docker exec -i ${COMPOSE_PROJECT_NAME}_${DATABASE}_slave mysql -u root --password=$MYSQL_PASSWORD $MYSQL_DATABASE < $FILE
    else
        # Arquivo SQL Zip
        pv $FILE | gunzip | docker exec -i ${COMPOSE_PROJECT_NAME}_${DATABASE} mysql -u $MYSQL_USER --password=$MYSQL_PASSWORD $MYSQL_DATABASE
    fi

else 
    echo "$FILE não existe."
fi