TEST=ON
start:
	docker-compose up -d --build

healthcheck:
	docker-compose run --rm healthcheck

down:
	docker-compose down

install: start healthcheck

configure:
	docker-compose -f docker-compose.yml -f wp-auto-config.yml run --rm wp-auto-config

autoinstall: start
	docker-compose -f docker-compose.yml -f wp-auto-config.yml run --rm wp-auto-config

clean: down
	@echo "💥 Removing related folders/files..."
	@sudo rm -rf www && sudo rm -rf .docker/data/mysql && mkdir www

dump: down
	@echo "Dump database"
	./dump-db.sh

reset: clean

buildbegin:
	@if [ "$(TEST)" = "ON" ]; then echo "PASSED"; else echo ""; fi