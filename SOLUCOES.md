# Soluções para problemas encontrado.
## Permissão nos arquivos shell *(.sh)*
```bash
# partindo da pasta principal
sudo chmod +x ./autoinstall.sh && 
sudo  chmod +x ./start.sh && 
sudo chmod +x ./stop.sh && 
sudo chmod +x ./update-db.sh && 
sudo  chmod +x ./dump-db.sh && 
sudo chmod +x ./clear.sh && 
sudo chmod +x ./dbslave.sh 
```

## Problemas de permissão para a pasta www/
```bash
sudo chown -R www-data:www-data www/ && sudo chown -R $USER:$USER www/
#ou
sudo chown -R www-data www/ && sudo chown -R $USER www/
```

## Permissão para pasta /plugins e /cache/*
```bash
sudo chmod 775 www/wp-content/plugins/ -R && 
sudo chmod 777 www/wp-content/themes/portaluniversal/cache/ -R && 
sudo chmod 777 www/wp-content/themes/portaluniversal/modulos/analytics/cache/ -R
```


## Apagar todo o wordpress e banco de dados e recriar pasta /www, caso o ./clear.sh não funcione
```bash
sudo rm -rf www && sudo rm -rf .docker/data/mysql && sudo rm -rf .docker/logs/apache2 && sudo rm -rf .docker/logs/mysql && sudo rm -rf .docker/cache/wp_cli && mkdir www
```

## Apagar cache do tema Sage
```bash
sudo rm -rf www/wp-content/uploads/cache/ && mkdir  www/wp-content/uploads/cache/ && sudo chmod 777 www/wp-content/uploads/cache/ -R
```

## Error conexão ao banco
```bash
# Acesse o console do contêiner: docker exec -i CONTAINER bash
# Crie um novo usuário para todas as redes: CREATE USER 'root'@'%' IDENTIFIED BY 'tiger';
# Permitir todas as conexões: GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' WITH GRANT OPTION;
```